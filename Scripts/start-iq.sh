#! /bin/sh
source ~/.bashrc
source $bitbar_plugin_dir/config.cfg
cd $iq_dir
javaopts="-Xmx1024m -XX:MaxPermSize=128m"
java  $javaopts -jar nexus-iq-server-*.jar server config.yml > /dev/null 2>&1 &
