#!/bin/bash
source /Users/ilkkaturunen/bitbar-plugins/Scripts/config.cfg 

ps aux | grep nexus-professional-2 | grep -v grep >/dev/null 2>&1
[ $? -gt 0 ] && echo "Nexus 2 | color=red" || echo "Nexus 2 | color=green"
PORT="$(grep application-port $nexus2_dir/conf/nexus.properties)"
echo "---"
echo "${PORT}"
echo "Start Nexus 2 | color=#123def bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/start-nexus.sh terminal=false" 
echo "Stop Nexus 2 | color=purple bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/stop-nexus.sh terminal=false"
