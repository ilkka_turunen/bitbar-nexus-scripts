#!/bin/bash
source /Users/ilkkaturunen/bitbar-plugins/Scripts/config.cfg 

ps aux | grep nexus-iq-server | grep -v grep >/dev/null 2>&1
[ $? -gt 0 ] && echo "IQ Server | color=red" || echo "IQ Server | color=green"
PORT="$(grep port $iq_dir/config.yml | grep -v '#' | grep -v org.)"
echo "---"
echo "${PORT}"
echo "Start IQ Server | color=#123def bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/start-iq.sh terminal=false" 
echo "Stop IQ Server | color=purple bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/stop-iq.sh terminal=false"
