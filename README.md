# README #

This is a series of scripts that allows you to start/stop services from your OS X Top bar and to monitor their status / configuration. It is based on Bitbar.

![Screen Shot 2016-01-18 at 15.37.33.png](https://bitbucket.org/repo/a6zkjM/images/3461691234-Screen%20Shot%202016-01-18%20at%2015.37.33.png)

![Screen Shot 2016-01-18 at 15.38.01.png](https://bitbucket.org/repo/a6zkjM/images/1346843091-Screen%20Shot%202016-01-18%20at%2015.38.01.png)

### What is this repository for? ###

* Monitor, start and stop Jenkins, Nexus 2, Nexus 3 and IQ server
* Version 1.0 Milestone 1 release pack 3 patch 3

### How do I get set up? ###

* Download and install [bitbar](https://github.com/matryer/bitbar)
* Clone this repository to a desired location
* Whilst in a terminal in the cloned repository, run this command to make all the files executable:

```
#!bash

chmod -R +x ./ *.sh
```
* Configure Bitbar to use the repo directory as plugin directory
* Run this command to add your plugin dir to your bashrc for easier access:

```
#!bash

echo 'export bitbar_plugin_dir="/BITBAR/PLUGIN/DIR/PATH/HERE/REPLACE/ME"' >> ~/.bashrc
```

* Modify **Scripts/config.cfg** to point to the right directories
* Start Bitbar
* ???? Magic


### How does it work? ###

Bitbar is a quite simple utility that uses stdin to parse widgets for your top bar. The shell scripts on the root of this projects represent the top bar widgets. The scripts under the Scripts-subdirectory contain configuration files and utility scripts to perform actions as seen in the dropdown menu.

The scripts in this directory need to be chmodded to be executable