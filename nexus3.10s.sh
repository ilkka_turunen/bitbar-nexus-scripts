#!/bin/bash
source /Users/ilkkaturunen/bitbar-plugins/Scripts/config.cfg 

ps aux | grep nexus-3 | grep -v grep >/dev/null 2>&1
[ $? -gt 0 ] && echo "Nexus 3 | color=red" || echo "Nexus 3 | color=green"
PORT="$(grep application-port $nexus3_dir/etc/org.sonatype.nexus.cfg)"
echo "---"
echo "${PORT}"
echo "Start Nexus 3 | color=#123def bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/start-n3.sh terminal=false" 
echo "Stop Nexus 3 | color=purple bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/stop-n3.sh terminal=false"