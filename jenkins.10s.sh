#!/bin/bash
source /Users/ilkkaturunen/bitbar-plugins/Scripts/config.cfg 

ps aux | grep tomcat | grep -v grep >/dev/null 2>&1
[ $? -gt 0 ] && echo "Jenkins| color=red" || echo "Jenkins | color=green"
echo "---"
echo "Port: 8080"
echo "Start Jenkins | color=#123def bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/start-jenkin.sh terminal=false" 
echo "Stop Jenkins | color=purple bash=/Users/ilkkaturunen/bitbar-plugins/Scripts/stop-jenkins.sh terminal=false"
